function (EmailService, config, Q, _) {
    return function() {
        var targetObj = this;
        if(!this.code || !this.user || !this.email) {
            throw new Error('Missing required field(s)');
        }
        
        return Q.all([
            config.getParameter('addon.email', {}),
            config.getParameter('sys.urlConfig', false)
        ]).then(resultArr =>{
            var resetConfig = resultArr[0].passwordReset;
            var serverUrl = _.get(resultArr[1], 'instanceUrl');
            
            var transportId = resetConfig.transport;
            var messageId = resetConfig.message;
            var emailAddress = this.email;
            
            if(!transportId || !messageId) {
                throw new Error('invalid addon.email.passwordReset config; missing transport and/or message ids');
            }
            
            var resetLink = serverUrl+'/dbui2/forgot_pw.html?c='+this.code;
            var subject = resetConfig.subject_line || 'Password Reset Request';
            var templateParams = {
                reset_link:resetLink, 
                user:this.user, 
                subject_line:subject  
            };
            return EmailService.sendEmail(transportId, messageId, templateParams, [emailAddress]);
        });
    }
}