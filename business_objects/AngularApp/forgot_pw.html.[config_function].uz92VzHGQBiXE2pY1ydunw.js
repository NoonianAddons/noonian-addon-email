function ($controllerProvider, $locationProvider) {
    
    $locationProvider.html5Mode(true);
    
    $controllerProvider.register('ForgotController', function($scope, $http, $location) {
        
        $scope.postData = {};
        
        
        var resetCode = $location.search().c;
        
        if(resetCode) {
            $http({
                method:'GET',
                url:'ws/sys/validatePasswordReset?code='+resetCode
            }).then(
                //Success:
                function(resp) {
                    // console.debug('WS response: ', resp);
                    if(resp.data) {
                        
                        if(resp.data.user) {
                            $scope.message = 'Please type in your new password below';
                            $scope.showResetForm = true;
                        }
                        if(resp.data.error === '$invalid-code') {
                            $scope.message = 'Code is either invalid or expired.  Please retry';
                            $scope.showUsername = true;
                        }
                    }
                    else {
                        $scope.message('An error occurred.');
                    }
                    
                },
                //ERROR
                function(resp) {
                    console.log('ERROR', resp);
                    $scope.messageLevel = 'alert-danger';
                    if(resp.data && resp.data.error) {
                        $scope.message = resp.data.error;
                    }
                    else {
                        $scope.message = 'An error occurred.';   
                    }
                }
            );
        }
        else {
            $scope.message = "Please enter username below and click 'Submit' to reset your password.";
            $scope.showUsername = true;
            
        }
        
        
        $scope.submitReq = function() {
            $scope.waiting = true;
            $http({
                method:'POST',
                url:'ws/sys/passwordReset',
                data:$scope.postData
            }).then(
                //Success:
                function(resp) {
                    // console.debug('WS response: ', resp);
                    $scope.waiting = false;
                    let respData = resp && resp.data || {result:'error', error:'Bad response from server.'};
                    
                    let r = $scope.submitResult = respData.result;
                    $scope.message = respData.message || respData.error;
                    
                    if(r === 'success') {
                        $scope.showUsername = false;
                        $scope.messageLevel = 'alert-success';
                        $scope.postData.prr_id = respData.prr_id;
                        $scope.showResubmitLink = true;
                    }
                    else {
                        console.error('error from sys/passwordReset', respData);
                        $scope.messageLevel = 'alert-danger';
                    }
                    
                },
                //ERROR
                function(resp) {
                    console.error('error thrown: sys/passwordReset', resp);
                    $scope.waiting = false;
                    let respData = resp && resp.data || {error:'Unknown error from server.'};
                    $scope.message = respData.error;
                    $scope.messageLevel = 'alert-danger';
                }
            );
            
        };
        
        $scope.submitPw = function() {
            $scope.waiting = true;
            var paramObj = {code:resetCode, password:$scope.postData.pw};
            $http.post('ws/sys/passwordReset', paramObj).then(
              function(resp) {
                console.info('WS response: ', resp);
                $scope.waiting = false;
                var responseObj = resp.data;
                if(responseObj.error) {
                    if(responseObj.error == '$complexity_requirements') {
                        $scope.message = 'Password does not meet complexity requirements';
                        $scope.complexityRequirements = responseObj.requirements;
                    }
                    else if(responseObj.error == '$') {
                        $scope.message = '';
                    }
                  
                }
                else if(responseObj.result === 'success') {
                    $scope.message = 'Successfully changed password';
                    $scope.showResetForm = false;
                    $scope.complexityRequirements = null;
                    $scope.showLoginLink = true;
                }
              },
              function(err) {
                $scope.waiting = false;
                var responseObj = err.data;
                if(responseObj) {
                   $scope.message = responseObj.error || err;
                };
              }
            );
        
        };
        
        
    })
}