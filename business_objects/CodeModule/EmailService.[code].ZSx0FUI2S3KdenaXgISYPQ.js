function (db, config, Q, _) {
    const exports = {};
    const nodemailer = require('nodemailer');
    const handlebars = require('handlebars');
    const moment = require('moment'); 
    
    const gridFs = db._svc.GridFsService;
    
    //From gridfs attachments create a list of nodemailer attachments
    //
    const buildAttachmentsList = function(attachmentList) {
        var promiseChain = Q(true);
        const ret = [];
        
        const addFile = function(id, name) {
            return gridFs.getFile(id).then(r=>{
                ret.push({
                    content:r.readstream,
                    filename:name
                });
            });
        }
        _.forEach(attachmentList, att=>{
            promiseChain = promiseChain.then(addFile.bind(null,att.attachment_id,att.filename));
        });
        promiseChain = promiseChain.then(()=>{
            return ret;
        });
        return promiseChain;
    };
    
    exports.sendEmail = function(transportId, messageId, contextObj, recipients, attachments, options) {
        console.log('email %s %s %j', transportId, messageId, contextObj);
        var deferred = Q.defer();
        
        var transportObj;
        var messageObj;
        
        var addonConfig;
        
        
        Q.all([
            db.EmailTransport.findOne({$or:[{_id:transportId},{key:transportId}]}), 
            db.EmailMessage.findOne({$or:[{_id:messageId},{key:messageId}]}),
            config.getParameter('addon.email', {})
        ])
        .then(function(resultArr) {
            transportObj = resultArr[0];
            messageObj = resultArr[1];
            addonConfig = resultArr[2];
            
            if(!transportObj) {
                throw deferred.reject('Invalid transport id '+transportId);
            }
            if(!messageObj) {
                throw deferred.reject('Invalid message id '+messageId);
            }
            if(!messageObj.recipients && !recipients) {
                throw deferred.reject('Message '+messageId+' missing recipient list');
            }
            
            const passAlong = [];
            
            if(messageObj.recipients) {
                passAlong.push(db.EmailRecipientList.findOne({_id:messageObj.recipients._id}).exec());
            }
            else {
                passAlong.push(false);
            }
            
            if(messageObj.attachments) {
                passAlong.push(buildAttachmentsList(messageObj.attachments));
            }
            else {
                passAlong.push(false);
            }
            
            return Q.all(passAlong);
            
        })
        .then(function([recipientListObj, msgAttachmetList]) {
            if(!recipients && !(recipientListObj && recipientListObj.addresses && recipientListObj.addresses.length)) {
                throw deferred.reject('No recipients for message '+messageId);
            }
            
            recipients = recipients || [];
            if(recipientListObj && recipientListObj.addresses && recipientListObj.addresses.length) {
                recipients = recipients.concat(recipientListObj.addresses);
            }
            
            attachments = attachments || [];
            if(msgAttachmetList && msgAttachmetList.length) {
                attachments = attachments.concat(msgAttachmetList);
            }
            
            return [recipients, attachments];
        })
        .then(function([emailAddressArr, attachmentArr]) {
            console.log(emailAddressArr);
            if(!emailAddressArr || emailAddressArr.length === 0) {
                throw deferred.reject('No recipients found for message '+messageId);
            }
            
            var transporter = nodemailer.createTransport(transportObj.config);
            var mailObj = transportObj.options || {};
            options && _.assign(mailObj, options);
            mailObj.to = emailAddressArr.join();
            
            var textBody = messageObj.text_body || messageObj.body;
            var htmlBody = messageObj.html_body;
            
            if(contextObj) {
                var subjTemplate = handlebars.compile(messageObj.subject);
                var textTemplate = textBody && handlebars.compile(textBody);
                var htmlTemplate = htmlBody && handlebars.compile(htmlBody);
                mailObj.subject = subjTemplate(contextObj);
                
                if(addonConfig.subjectLinePrefix) {
                    mailObj.subject = addonConfig.subjectLinePrefix + ' ' + mailObj.subject;
                }
                
                if(textBody) {
                    mailObj.text = textTemplate(contextObj);
                }
                if(htmlBody) {
                    mailObj.html = htmlTemplate(contextObj);
                }
            }
            else{
                mailObj.subject = messageObj.subject;
                if(textBody) {
                    mailObj.text = textBody;
                }
                if(htmlBody) {
                    mailObj.html = htmlBody;
                }
            }
            
            if(attachmentArr && attachmentArr.length) {
                mailObj.attachments = attachmentArr;
            }
            
            console.log('sending %j', mailObj);
            transporter.sendMail(
                mailObj, 
                function(err, info) {
                    if(addonConfig.saveSentMessages) {
                        const sentMsgObj = new db.SentEmailMessage({
                            sent_ts:moment().format(),
                            email_message:{_id:messageObj._id},
                            email_transport:{_id:transportObj._id},
                            to:emailAddressArr,
                            from:mailObj.from,
                            subject:mailObj.subject,
                            html_body:mailObj.html,
                            text_body:mailObj.text
                        });
                        if(mailObj.attachments) {
                            sentMsgObj.attachments = _.pluck('filename');
                        }
                        if(err) {
                            sentMsgObj.status = 'failure';
                            sentMsgObj.result = {error:err};
                            
                        }
                        else {
                            sentMsgObj.status = 'success'
                            sentMsgObj.result = info;
                        }
                        
                        sentMsgObj.save().then(()=>{
                            err ? deferred.reject(err) : deferred.resolve(info);
                        });
                        return;
                    }
                    
                    if(err) {
                        return deferred.reject(err);
                    }
                    deferred.resolve(info);
                }
            );
        })
        ;
        
        
        return deferred.promise;
    };
    
    //THANK YOU: http://worsethanfailure.com/articles/Validating_Email_Addresses
    exports.validAddress = function(add) {
        var ampisthere = false;
        var spacesthere = false;
        
        var textbeforeamp = false;
        var textafteramp = false;
        var dotafteramp = false;
        var othererror = false;
        
        for(var i = 0; i < add.length; ++i) {
            if(add.charAt(i) == '@') {
                if(ampisthere)
                    othererror = true;
            
                ampisthere = true;
            }
            else if(!ampisthere) {
                textbeforeamp = true;
            }
            else if(add.charAt(i) == '.') {
                dotafteramp = true;
            }
            else {
                textafteramp = true;
            }
            
            if(add.charAt(i) == ' ' || add.charAt(i) == ',') {
                spacesthere = true;
            }
        }
        
        if(spacesthere || !ampisthere || !textafteramp || !textbeforeamp || !dotafteramp || othererror)
        {
            return false;
        }
        
        return true;
        
        
    };
    
    return exports;
}