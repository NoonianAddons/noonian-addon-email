function (EmailService, config, db, Q, _) {
    //Generate a code, send the email
    
    if(this.email && this.user) {
        this.code = db.generateId(); 
        
        //absorb any error here and just log
        return this.sendEmail().then(result=>{
            console.debug('pw reset email result: %j', result);
            this.send_attempt_result = result;
        }, 
        err=> {
            this.send_attempt_result = {error:err};
            console.error('error sending pw reset email: %j', err);
        });
    }
    else {
        this.send_attempt_result = "not attempted; missing field email and/or user";
    }
    
}